#!/bin/bash

# assumes a previous maven run to install all jars into local maven repo
# run from the root of agave project
#  mvn -s config/maven/settings-SAMPLE.xml -f pom.xml  -P agave,plain  clean install

WORKSPACE=$1
cd $WORKSPACE

# cleaner way to get updates
if [ ! -d "agave-apps" ]; then
  git clone https://bitbucket.org/agaveapi/agave.git --recursive -b slt-build .
fi

docker-machine ssh slt-b2d df -h

#git checkout slt-build
#git pull origin slt-build

#git submodule foreach git checkout slt-build
#git submodule foreach git pull origin slt-build

# create the aggregate testreports folder
if [ ! -d "testreports" ]
then
     mkdir testreports
else
     # clean out testreports
     rm -rf testreports/*
fi


echo "*******************************************************************"
echo "* Notifications"
echo "*******************************************************************"
config/testbed/provision-utests.sh notifications  "mysql beanstalkd mongodb" testng.xml
sleep 10
echo ""

#echo "*******************************************************************"
#echo "* Profiles"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh profiles  "mysql" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Metadata"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh metadata  "mysql mongodb beanstalkd" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Systems model"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh systems  "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng-model.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Systems remote"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh systems  "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng-remote.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Systems transfers"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh systems  "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng-transfers.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Systems permissions"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh systems  "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng-permissions.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Systems performance"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh systems  "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng-performance.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Monitors"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh monitors "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Files"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh files "mysql mongodb beanstalkd myproxy sftp irods irodspam irods4 http ftp" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Transforms"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh transforms "mysql mongodb beanstalkd" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Apps"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh apps "mysql mongodb" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Jobs"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh jobs "mysql mongodb" testng.xml
#sleep 10
#echo ""

#echo "*******************************************************************"
#echo "* Realtime"
#echo "*******************************************************************"
#config/testbed/provision-utests.sh realtime "mysql mongodb" testng.xml
#sleep 10
#echo ""

sleep 10
echo " ***** cleaning up after test run ****** "
docker ps
echo " stopping unit test containers if any  "
docker stop $(docker ps -f label=agaveapi.test -q)
sleep 10

echo " removing stopped unit test containers "
docker rm -f $(docker ps -f label=agaveapi.test -aq)
sleep 10

echo " clean up docker space "
docker run -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/docker:/var/lib/docker --rm martin/docker-cleanup-volumes
echo "  disk space :  "
docker-machine ssh slt-b2d df -h
echo ""
echo "******* ending unit test run  $(date)  *******"
