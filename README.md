# README #

### What is this repository for? ###

* Proof of concept for Agave source code repository removed from submodules model. 
* Version

### General steps ###

* Create a directory to hold agave-fork-hybrid and all the repositories related
* Clone this repository
* Clone the following 
* *  agave-apidocs
* *  agave-apps
* *  agave-auth
* *  agave-common
* *  agave-files
* *  agave-jobs
* *  agave-logging
* *  agave-metadata
* *  agave-migrations
* *  agave-monitors
* *  agave-notifications
* *  agave-postits
* *  agave-profiles
* *  agave-realtime
* *  agave-stats
* *  agave-systems
* *  agave-tags
* *  agave-tenants
* *  agave-transforms
* *  agave-usage